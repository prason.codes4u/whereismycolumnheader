from distutils.core import setup

setup(name='whereismyheader',
      version='1.0',
      description='Header detection module in excel and csv files',
      author='Prason Ghimire',
      author_email='meet.prasonghimire@gmail.com',
      package_dir={"":"src"},
      requires=["pandas>=1.4.0","openpyxl==3.0.10","scikit-learn==1.1.1"]
)