import math
from itertools import islice

def calculate_range_index(total_lines:int):
    range_initial=f"0:{math.ceil(total_lines*.15)}"
    range_mid=f"{math.ceil(total_lines*.5)}:{math.ceil(total_lines*.6)}"
    range_end=f"{math.ceil(total_lines*.85)}:{total_lines}"
    return (range_initial,range_mid,range_end)

def slice_range_data(lines:list,range_initials,range_mid,range_end):
    data_initials=eval(f"lines[{range_initials}]")
    data_mid=eval(f"lines[{range_mid}]")
    data_end=eval(f"lines[{range_end}]")
    return (data_initials,data_mid,data_end) 

def validatePathInput(filePath:str):
    extension=filePath.lower().rsplit('.',1)[1]
    if extension in ['xlsx','csv']:
        return extension
    else:
        raise Exception("Invalid File Path:: Only 'csv' and 'xlsx' files are supported as of Now.")


