from collections import Counter
import imp
import pandas as pd
from configs import pglogs
from featureminer.datahandler import calculate_range_index,slice_range_data
from featureminer import custom_obj_evaluator
from model import HeaderClassifierModel

class CustomExcelMiner:

    def __init__(self,filePath:str,howmanytopredict:int):
        self.filePath=filePath
        self.howmanytopredict=howmanytopredict

    def get_data_ranges_for_excel(self):
        try:
            excel_df=pd.read_excel(self.filePath,header=None)
            lines=excel_df.head(10000).values    
            range_initials,range_mid,range_end=calculate_range_index(len(lines))
            data_initials,data_mid,data_end=slice_range_data(lines,range_initials,range_mid,range_end)
            return {'data':{'data_initial':data_initials,'data_mid':data_mid,'data_end':data_end},
                    'ranges':{'initial_range':range_initials,'middle_range':range_mid,'end_range':range_end}
                }
        except FileNotFoundError as fnf:
            pglogs.error(f"Missing file: {fnf}")
        except Exception as ex:
            pglogs.error(f"Failed during the range calculation of excel: {ex}")

    def calculate_most_common_columsize(self,datalist):
        try:
            if datalist:
                mycounter=Counter()
                for data_value in datalist['data'].values():
                    for nested_data in data_value:
                        ab=filter(lambda x : pd.notna(x),nested_data)
                        mycounter.update(Counter([len(list(ab))]))
                return mycounter.most_common()[0][0]
            else:
                return 0
        except KeyError as krr:
            preffered_format_sample={'data':{'data_initial':[],'data_mid':[],'data_end':[]},
            'ranges':{'initial_range':'0:5','middle_range':'10:15','end_range':'40:50'}}
            raise Exception(f'Invalid structure for the data supplied. Use the structure as :{preffered_format_sample}')

    def feature_extractor_for_header_classifier(self,databatch,starter=1):
        dtype_down=[]
        for row in databatch:
            try:
                data=[custom_obj_evaluator(d) for d in row if pd.notna(d)]
                df=pd.DataFrame([data])
                dtype_down.append(list(df.dtypes))
            except Exception as ex:
                pglogs.error(f"Type Inferencing error : Provide data with the value excell Null or None or NaN :: {ex} ")
                return 

        features_list=[]
        for type_index,type_infer in enumerate(dtype_down):
            actual_col_count=len(type_infer)
            match_accross_row=all([type_infer[actual_col_count-1]==d for d in type_infer[:-1]])
            match_below_row=all([type_infer==d for d in dtype_down[type_index:]])    
            features_list.append([(starter+type_index),actual_col_count,match_accross_row,match_below_row])
        return features_list

    def identify_header_from_file_excel(self):
        pglogs.info('..........Starting the Header inference Engine.........\n\n')
        
        pglogs.info('Step 1: Starting the data ranges estimation for feature extraction')
        raw_batch_for_feature_extractions=self.get_data_ranges_for_excel()
        if raw_batch_for_feature_extractions is None:
            pglogs.info("Process Aborted due to Error, Please refer to logs")
            return 
        pglogs.info("\t\tBatch Creation Successful...\n\n")
        pglogs.info("Step 2: Estimating the expected Column size of your file......")
        expected_col_count=self.calculate_most_common_columsize(raw_batch_for_feature_extractions)
        pglogs.info(f"\t\tEstimation Successful:{expected_col_count} column(s) expected\n\n")
        
        pglogs.info('Step 3: Initiating the Feature Exctracation from batches created at step 1.')
        #now we have to run the classification for each row of each batch so,
        prediction_cols=[]
        for index,batch in enumerate(raw_batch_for_feature_extractions['data'].values()):
            pglogs.info(f'\t\tStep 3.{(index+1)} : extracting from {(index+1)}th batch.')
            feature_list=self.feature_extractor_for_header_classifier(batch,
                                                    int(list(raw_batch_for_feature_extractions['ranges'].values())[index].split(":")[0])
                                                )
            pglogs.info(f'\t\t\textraction Complete {(index+1)}th batch.')
            pglogs.info(f'\t\t\tRunning models for {(index+1)}th batch.')
            for feature in feature_list:
                feature.insert(1,expected_col_count)
                result =HeaderClassifierModel.trigger_header_classifier_model(feature)
                if result is not None:
                    prediction_cols.append(result)
                    if len(prediction_cols)==self.howmanytopredict:
                        pglogs.info(f'Step 4: Detection Success. Check the result')
                        return str(f"Header of this file start from row : {prediction_cols}")
                        
                else:
                    continue
            if prediction_cols:
                return str(f"Header of this file start from row : {prediction_cols}")
            else:
                pglogs.info(f'\t\t\t\t\t\t No Header Detection in {(index+1)}th batch.')
        
        pglogs.info(f'\t\t\t\t No Header Detection in Any batch.')
        pglogs.info('Step 4: Detection Module terminated.please upload proper file with Header information')
    