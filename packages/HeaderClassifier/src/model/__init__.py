class HeaderClassifierModel:

    @staticmethod
    def trigger_header_classifier_model(feature:list):
        import pickle,os
        import warnings
        warnings.filterwarnings('ignore')
    #     with open('./header_scaler.pkl','rb') as modelScaler:
    #         myscaler=pickle.load(modelScaler)
        
    #     print(feature[1:3],'for transorm of scaling')
    #     scaled_value=myscaler.transform([feature[1:3]])
    #     transformed_data=scaled_value[0].tolist()
    #     transformed_data.extend(feature[3:])
        transformed_data=feature[1:]
        with open(os.path.abspath('./HeaderClassifier/src/model/header_classifier.pkl'),'rb') as regression_model:
            header_classifier_model=pickle.load(regression_model)
        try:
            result=header_classifier_model.predict([transformed_data])
            if result:
                # print(header_classifier_model.predict_proba([transformed_data]))
                return feature[0]
            
            return None
        except Exception as ex:
            raise Exception(f"Exception during the model prediction :: {ex}")