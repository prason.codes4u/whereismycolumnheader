from featureminer.datahandler import validatePathInput
from featureminer.csv import CustomCSVMiner
from featureminer.excel import CustomExcelMiner
from configs import pglogs
import pandas as pd

class PrasonHeaderDetector:

    def __init__(self,filePath:str,seperator=',',howmanytopredict=1) -> None:
        self.filePath=filePath
        self.seperator=seperator
        self.howmanytopredict=howmanytopredict

    
    def trigger_detection(self):
        try:
            extension=validatePathInput(self.filePath)
            if extension=='xlsx':
                miner=CustomExcelMiner(self.filePath,self.howmanytopredict)
                abc=miner.identify_header_from_file_excel()
            elif extension=='csv':
                miner=CustomCSVMiner(self.filePath,self.seperator,self.howmanytopredict)
                abc=miner.identify_header_from_file()
            else:
                raise Exception("Invalid file type upload. please upload only csv or excel file")
            
            predictedHeaders=eval(abc.rsplit(':')[1].strip())
            output={'total_headers':len(predictedHeaders)}
            for index,headerNo in enumerate(predictedHeaders):
                if extension=='csv':
                    output_df=pd.read_csv(self.filePath,header=headerNo,skip_blank_lines=False)
                elif extension=='xlsx':
                    output_df=pd.read_excel(self.filePath,header=headerNo)
                output.update({f"header_{index+1}":list(output_df.columns)})
            return output
        except Exception as ex:
            pglogs.error(f"Prediction Error :: {ex}")
            pass