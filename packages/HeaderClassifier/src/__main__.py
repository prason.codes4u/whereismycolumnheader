from detector import PrasonHeaderDetector

def main():
    filePath='D:/Bridge Projects/metamanager_testfiles/bpmsamplereports/Data Export_Brita_2019_2020.xlsx'
    # filePath='D:/Bridge Projects/metamanager_testfiles/bpmsamplereports/HiddenValley_FY20-AMJ-HVX-Shaker_platform-May-1-2020-May-31-2020.csv'
    detector=PrasonHeaderDetector(filePath)
    columns=detector.trigger_detection()
    print(columns)

if __name__=='__main__':
    print('iside main')
    main()
